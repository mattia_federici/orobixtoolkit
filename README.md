# OrobixToolkit 
As the name said

## Getting Started

Clone the repo:
```
git clone https://mattia_federici@bitbucket.org/orobix/steam.git
```
or
```
python -m pip install git+https://mattia_federici@bitbucket.org/mattia_federici/orobixtoolkit.git
```
### Requirements.txt
```
torch>=1.4.0
pandas>=1.0.3
h5py>=2.10.0
pycocotools>=2.0.0
scikit-learn>=0.22.1
torchvision>=0.5.0
numpy>=1.17.3
Pillow>=7.0.0
matplotlib>=3.2.1
scikit-image>=0.16.2
configparser>=5.0.0
tqdm>=4.41.1
imgaug>=0.4.0
opencv-python>=4.2.0.32
mlflow>=1.7.2
Hydra>=2.5
torchsummary>=1.5.1
minio>=5.0.8
```