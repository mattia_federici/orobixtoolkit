from OxImage.segmentation_utils import resize
from torch.utils.data import Dataset
import pandas as pd
import os
import json
from OxImage.segmentation_utils import *
import numpy as np
import h5py
import imgaug.augmenters as iaa
from OxImage.image_utils import *
import torch
from sklearn.model_selection import train_test_split

classes = {
    "mouse": 0,
    "none": 1,
}


class MouseDataset(Dataset):
    def __init__(self, partition):
        if not os.path.exists("mouse.csv"):
            init_mouse_dataset()
        df = pd.read_csv("mouse.csv", index_col=0)
        df = df[df["partition"] == partition]
        df.reset_index(inplace=True)
        self.partition = partition
        self.img_names = ("mouse/img/"+df["file"]).to_list()
        self.label = df["defect"].apply(lambda x: classes.get(x, 1)).to_list()

        df[["x", "y", "width", "height"]] = df[["x", "y", "width", "height"]].astype(float)
        self.bb = np.stack([df["x"].to_numpy(), df["y"].to_numpy(), df["width"].to_numpy(), df["height"].to_numpy()], axis=1)
        self.bb = np.ascontiguousarray(np.array(self.bb))
        self.num_classes = 1
        self.aug = iaa.Sequential([
            # iaa.Sometimes(.5, iaa.contrast.LinearContrast((0.95, 1.05))),
            # iaa.Sometimes(.5, iaa.MultiplyBrightness((0.95, 1.05))),
            iaa.Sometimes(.5, iaa.Affine(
                translate_percent={"x": (-0.05, 0.05), "y": (-0.05, 0.05)},
            )),
            iaa.Sometimes(.5, iaa.Affine(
                rotate=(-5, 5),
            )),
            iaa.Fliplr(.5),
            iaa.Flipud(.5),
            # iaa.Sometimes(.5, iaa.Cutout()),
            iaa.Sometimes(.5, iaa.Rot90([0, 1, 2, 3]))
        ], random_order=True)
        self.base_img = np.copy(read_image_ski(self.img_names[0]))

    def __getitem__(self, index):
        img = get_img_from_h5py(self.img_names[index])
        y = self.label[index]
        bb = self.bb[index]
        bb = torch.from_numpy(bb).float().unsqueeze(0)
        bb = transform_bb(bb, form_in="hw", form_out="point")
        base, bb = resize(self.base_img, 512, 512, bounding_boxes=bb, bounding_boxes_form="point")
        if self.partition == "train":
            img, bb = sequencer(self.aug, img, bounding_boxes=bb, segmentation_mask=None)

        #plot_img_with_bb(img, bb, boxes_form="point", save_img=True, saved_img_path="pp.jpg")
        bb = torch.from_numpy(bb/512.)
        bb.clamp_(min=0, max=1)
        return torch.from_numpy(np.ascontiguousarray(img)).float(), np.asarray([y]),  bb, self.img_names[index]

    def __len__(self):
        return len(self.img_names)


def get_img_from_h5py(img_name):
    with h5py.File("mouse.hdf5", "r") as f:
        image = f[img_name][:]
    return image

def detection_collate(batch):
    """
    Dataloader custom collate function for dealing with batches of images that have a different
    number of associated object annotations (bounding boxes).

    :param batch: (tuple) A tuple of tensor images and lists of annotations
    :return a tuple containing:
        1) (tensor) batch of images stacked on their 0 dim
        2) (list of tensors) annotations for a given image are stacked on 0 dim
    """
    targets = []
    imgs = []
    bb = []

    img_name = []
    for sample in batch:
        imgs.append(sample[0])
        targets.append(sample[1])
        bb.append(sample[2])
        img_name.append(sample[3])
    return torch.stack(imgs, 0), targets, bb, img_name


def init_mouse_dataset():
    """
    import os
    import OxDataset
    from zipfile import ZipFile
    path = os.path.abspath(OxDataset.__file__).split("__ini")[0]
    rs = os.path.join(path, "MouseDataset/mouse.7z")
    with ZipFile(rs, 'r') as zipObj:
        # Extract all the contents of zip file in current directory
        zipObj.extractall()
    """
    result = "mouse/Risultati.csv"
    classifier = "mouse/img/autoCsvClassifier.csv"
    result_df = pd.read_csv(result, delimiter=";", names=["uuid", "op", "defect"], usecols=["uuid", "defect"])
    result_df["defect"] = result_df["defect"].apply(json.loads)
    result_df = result_df.explode("defect")
    result_df.dropna(inplace=True)
    json_df = pd.json_normalize(result_df["defect"])
    result_df.drop(columns="defect", inplace=True)
    result_df = pd.concat([result_df, json_df], axis=1)
    class_df = pd.read_csv(classifier, usecols=["uuid", "file"])
    df = pd.merge(result_df, class_df, on="uuid").reset_index()
    train, val = train_test_split(df.index)
    df.loc[train, "partition"] = "train"
    df.loc[val, "partition"] = "validation"
    df.to_csv("mouse.csv")
