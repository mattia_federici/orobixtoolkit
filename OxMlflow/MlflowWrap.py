import os
from minio import Minio
import mlflow
import configparser

# Minio server address.
MLFLOW_S3_ENDPOINT_URL = "http://192.168.3.77:9000"
MINIO_ENDPOINT = "192.168.3.77:9000"

MINIO_ACCESS_KEY = "minio_mlflow"
MINIO_SECRET_KEY = "minio_mlflow"
MINIO_BUCKET = "mlflow"
# MLflow server address.
MLFLOW_TRACKING_URI = "http://192.168.3.77:5000"


class MlflowWrap:

    def __init__(self, experiment_name, run_name):
        set_minio_credentials()
        create_minio_bucket()

        mlflow.set_tracking_uri(MLFLOW_TRACKING_URI)
        mlflow.set_experiment(experiment_name)
        mlflow.start_run(run_name=run_name)
        self.run_id = mlflow.active_run().info.run_id

    def get_run_id(self):
        return self.run_id

    @staticmethod
    def log_params(params):
        mlflow.log_params(params)

    @staticmethod
    def log_param(param, value):
        mlflow.log_param(param, value)

    @staticmethod
    def log_artifact(artifact):
        try:
            mlflow.log_artifact(artifact)
        except Exception as exc:
            print(exc)

    @staticmethod
    def log_metric(metric, value, step=None):
        mlflow.log_metric(metric, value, step)

    @staticmethod
    def log_params_from_parser(configuration_file_path):
        mlflow.log_params(get_dictionary_from_config_parser(configuration_file_path))


def get_dictionary_from_config_parser(file, divided_by_section=False):
    parser_raw = configparser.RawConfigParser()
    parser_raw.read_file(open(file))
    parser_dict = dict()

    sections = parser_raw.sections()

    for section in sections:
        options = parser_raw.options(section)
        temp_dict = {}
        for option in options:
            temp_dict[option] = parser_raw.get(section, option)

        if divided_by_section:
            parser_dict[section] = temp_dict
        else:
            parser_dict.update(temp_dict)

    return parser_dict


def set_minio_credentials():
    # Sets environment variables needed to boto3/minio.
    os.environ['AWS_ACCESS_KEY_ID'] = MINIO_ACCESS_KEY
    os.environ['AWS_SECRET_ACCESS_KEY'] = MINIO_SECRET_KEY
    os.environ['MLFLOW_S3_ENDPOINT_URL'] = MLFLOW_S3_ENDPOINT_URL


def create_minio_bucket():
    # Creates the Minio bucket for the artifacts.
    minio_client = Minio(endpoint=MINIO_ENDPOINT,
                         access_key=MINIO_ACCESS_KEY,
                         secret_key=MINIO_SECRET_KEY,
                         secure=False)

    if not minio_client.bucket_exists(MINIO_BUCKET):
        minio_client.make_bucket(MINIO_BUCKET)
