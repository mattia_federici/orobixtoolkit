from setuptools import setup, find_packages


def get_req():
    with open("requirements.txt", mode="r") as f:
        requirements = f.readlines()
    return requirements


setup(name='orobixtoolkit',
      version='0.2.51',
      description='Python Distribution Utilities for Orobix',
      author='Mattia Federici',
      author_email='mattia.federici@orobix.it',
      packages=find_packages(),
      license='MIT',
      python_requires=">3",
      install_requires=get_req(),
      )